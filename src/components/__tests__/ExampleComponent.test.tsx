import { render } from '@testing-library/react';
import { ExampleComponent } from '../ExampleComponent';
import React from 'react';

describe('empty test', () => {
    it('renders without crashing', () => {
        const { asFragment } = render(<ExampleComponent />);

        expect(asFragment()).toMatchSnapshot();
    });
});
