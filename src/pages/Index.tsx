import * as React from 'react';
import * as ReactDOM from 'react-dom';

ReactDOM.render(
    <div style={{ padding: '1em' }}>
        <h1>Index Page</h1>
    </div>,
    document.getElementById('overview'),
);
