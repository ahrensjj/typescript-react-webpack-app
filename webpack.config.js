const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const pagesPath = path.resolve(__dirname, 'src/pages');
const pageTemplatePath = path.resolve(pagesPath, 'index.html');


module.exports = (_, env) => {
    const devEnv = env.mode !== 'production';

    return {
        context: path.resolve(__dirname, 'src'),
        module: {
            rules: [
                {
                    test: /\.css$/,
                    use: [
                        "style-loader",
                        "css-loader"
                    ]
                },
                {
                    test: /\.tsx?$/, loader: "ts-loader"
                }
            ]
        },
        resolve: {
            extensions: ['.ts', '.tsx', '.js']
        },
        entry: {
            index: path.resolve(pagesPath, 'Index.tsx'),
            vendor: ['react', 'react-dom']
        },
        plugins: [
            new HtmlWebpackPlugin({
                filename: 'index.html',
                template: pageTemplatePath,
                inject: 'body',
                devEnv,
                chunks: ['index'],
                title: 'example-app',
                html: "<div id=\"overview\"/>"
            }),
        ],
        output: {
            path: path.join(__dirname, 'build/resources/main/public'),
            publicPath: '/example-app/',
            filename: chunkData => ('assets/scripts/[name].[hash].js')
        },
        devtool: 'source-map',
        devServer: {
            publicPath: '/example-app/',
            port: 3000,
            open: true,
            openPage: 'example-app/index.html',
            overlay: {
                warnings: false,
                erros: true
            },
            proxy: {
                '/example-app/webresources': {
                    target: 'http://localhost:10085'
                }
            }
        }
    }

};  