# Setup for this Project

## requirements

`NPM` and `Node` are already installed.

## Initialize project:

Run npm init in your project root directory.
```
npm init -y
```

Install Dependencies for `webpack`, `typescript` and `react`.
```
npm i --save-dev webpack webpack-cli webpack-dev-server html-webpack-plugin
npm i --save react react-dom
npm i --save-dev @types/react @types/react-dom
npm i --save-dev typescript ts-loader source-map-loader
```
## Configure Webpack

create `webpack.config.js` file.
- merged config for Page and Webcomponents für Prod und Test
- config für css und ts loader
- proxy für das eigene Backend und anderes wie cdn usnd uxp
- es gibt eine IndexPage und Webcomomponents

## Page Template

Create template `src/index.html`
```
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>React-Webpack Setup</title>
</head>
<body>
  <div id="overview"></div>
</body>
</html>
```

## Package.json

Add scripts in `package.json` : 
```json
"scripts": {
  "build:dev": "webpack-dev-server --mode=development",
  "build":"webpack --progress --mode=production"
}
```

## Configure typescript

Create `tsconfig.json`
```json
{
    "compilerOptions": {
        "target": "es6",
        "sourceMap": true,
        "noImplicitAny": true,
        "esModuleInterop": true,
        "module": "commonjs",
        "types": [
            "node"
        ],
        "lib": [
            "dom",
            "es6",
            "scripthost",
            "dom.iterable"
        ],
        "jsx": "react",
        "typeRoots": [
            "node_modules/@types"
        ]
    },
    "include": [
        "./src/**/*"
    ]
}
```

## Testing

For testing we will use `jest` and `react-testing-library`.

### Jest 

Install jest.
`npm install --save-dev jest ts-jest @types/jest`

#### Configure Jest

Create `jest.config.js` file:
```typescript
module.exports = {
    testURL: 'http://localhost/',
    moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json'],
    testRegex: 'test\.tsx?$',
    preset: 'ts-jest',
    testMatch: null,
};

```
Add jest to types in `tsconfig.json`
```json
"types": [
            "node",
            "jest"
],
```

Add script to `package.json`
```
"scripts": {
  "build:dev": "webpack-dev-server --mode=development",
  "build":"webpack --progress --mode=production",
  "test": "jest --coverage"
}
```

### React Testing Library

Install react testing library
```
npm install --save-dev @testing-library/jest-dom @testing-library/react
```

### Mocks

We use `jest-fetch-mock` in order to make mocking easier.
```
npm i --save-dev jest-fetch-mock
```

Create a `test-setup.ts` file.
```
import { GlobalWithFetchMock } from 'jest-fetch-mock';

// any shouldn't be necessary
const customGlobal: GlobalWithFetchMock = global as any as GlobalWithFetchMock;
customGlobal.fetch = require('jest-fetch-mock');
customGlobal.fetchMock = customGlobal.fetch;
```

Add `test-setup.ts` to `jest.config.js`.
```
setupFiles:[
        './test-setup.ts'
    ]
```

## ESLint

Install eslint, parser, typescript and react plugin.
(Source: https://www.robertcooper.me/using-eslint-and-prettier-in-a-typescript-project)
```
npm i eslint @typescript-eslint/parser @typescript-eslint/eslint-plugin eslint-plugin-react --save-dev
```

Create `.eslintrc.js` file.

```
module.exports = {
  parser: "@typescript-eslint/parser", // Specifies the ESLint parser
  parserOptions: {
    ecmaVersion: 2020, // Allows for the parsing of modern ECMAScript features
    sourceType: "module", // Allows for the use of imports
    ecmaFeatures: {
      jsx: true // Allows for the parsing of JSX
    }
  },
  settings: {
    react: {
      version: "detect" // Tells eslint-plugin-react to automatically detect the version of React to use
    }
  },
  extends: [
    "plugin:react/recommended", // Uses the recommended rules from @eslint-plugin-react
    "plugin:@typescript-eslint/recommended" // Uses the recommended rules from @typescript-eslint/eslint-plugin
  ],
  rules: {
    // Place to specify ESLint rules. Can be used to overwrite rules specified from the extended configs
    // e.g. "@typescript-eslint/explicit-function-return-type": "off",
  },
};
```
## Prettier

To use Prettier together with ESLint install the following.
- prettier: The core prettier library
- eslint-config-prettier: Disables ESLint rules that might conflict with prettier
- eslint-plugin-prettier: Runs prettier as an ESLint rule
```
npm i prettier eslint-config-prettier eslint-plugin-prettier --save-dev
```

Create `.prettierrc.js` file.
```
module.exports = {
  semi: true,
  trailingComma: "all",
  singleQuote: true,
  printWidth: 120,
  tabWidth: 4
};
```

Now the `.eslintrc.js` file has to be updated. These lines have to be added to the exents array:
```
extends: [
  <...>,
  "prettier/@typescript-eslint", // Uses eslint-config-prettier to disable ESLint rules from @typescript-eslint/eslint-plugin that would conflict with prettier
  "plugin:prettier/recommended" // Enables eslint-plugin-prettier and eslint-config-prettier. This will display prettier errors as ESLint errors. Make sure this is always the last configuration in the extends array.
]

```
The advantage of having prettier setup as an ESLint rule using eslint-plugin-prettier is that code can automatically be fixed using ESLint's --fix option.
Hint: Make sure that plugin:prettier/recommended is the last configuration in the extends array.

Finally add ESLint to the scripts in package.json
```
"lint": "eslint 'src/**/*.{js,ts,tsx}' --fix"
```

## Possible Todos

- Axios
- Routing
- React Hook Form
  https://stackoverflow.com/questions/58703615/react-hook-form-with-antd-styling


