module.exports = {
    testURL: 'http://localhost/',
    moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json'],
    testRegex: 'test\.tsx?$',
    setupFiles:[
        './test-setup.ts'
    ],
    preset: 'ts-jest',
    testMatch: null,
};